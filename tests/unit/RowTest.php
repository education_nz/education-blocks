<?php

namespace Education\Blocks\Tests;

use SilverStripe\Dev\SapphireTest;
use Education\Blocks\Row;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\ArrayList;

class RowTest extends SapphireTest
{
    public function testGetCMSFields()
    {
        $block = new Row();

        $this->assertInstanceOf(FieldList::class, $block->getCMSFields());
    }

    public function testGetType()
    {
        $block = new Row();

        $this->assertEquals('Row', $block->getType());
    }

    public function testGetRatio(){
        $block = new Row();
        $this->assertEquals(2, $block->getColumnCount());
        $this->assertEquals('1-1', $block->getColumnRatio());
        $this->assertFalse($block->getHasRatio(), 'No ratio when 2 columns and ratio 1-1');

        $block->setColumnRatio('1-2');
        $this->assertTrue($block->getHasRatio(), 'Has ratio when 2 columns and ratio not 1-1');

        $block->setColumnCount(3);
        $this->assertFalse($block->getHasRatio(), 'No ratio when more than 2 columns and ratio not 1-1');
    }

    public function testGetVisibleElements(){
        $block = new Row();

        $this->assertInstanceOf(ArrayList::class, $block->getVisibleElements(), 'Returns a list of elements');

        // todo test limiting
    }
}
