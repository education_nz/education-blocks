<?php

namespace Education\Blocks\Tests;

use SilverStripe\Dev\SapphireTest;
use Education\Blocks\News;
use SilverStripe\Forms\FieldList;

class NewsTest extends SapphireTest
{
    public function testGetCMSFields()
    {
        $block = new News();

        $this->assertInstanceOf(FieldList::class, $block->getCMSFields());
    }
}
