<?php

namespace Education\Blocks\Tests;

use SilverStripe\Dev\SapphireTest;
use SilverStripe\Forms\FieldList;
use Education\Blocks\Hero;

class HeroTest extends SapphireTest
{
    public function testGetCMSFields()
    {
        $block = new Hero();

        $this->assertInstanceOf(FieldList::class, $block->getCMSFields());
    }
}
