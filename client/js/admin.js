(function($) {
    $.entwine('ss', function($) {

        $('.stylingoptionset input').entwine({
            onmatch: function() {
                if (this.is(':checked')) {
                    this.check();
                } else {
                    this.uncheck();
                }
            },
            getLabel: function() {
                return $('label', this.parent());
            },
            onchange: function(e) {
                var $otherCheckboxes = $('input', this.closest('ul')).not(this);
                if (this.is(':checked')) {
                    $otherCheckboxes.uncheck();
                    this.check();
                } else {
                    $otherCheckboxes.check();
                    this.uncheck();
                }
            },
            check: function() {
                this.parent().addClass('ischecked');
            },
            uncheck: function() {
                this.parent().removeClass('ischecked');
            }
        });


        /* Handle the image type fields */
        $('#Form_ItemEditForm_ImageType').entwine({
            onmatch: function() {
                this.updateFields($('#Form_ItemEditForm_ImageType').val());
            },

            onchange: function(e) {
                this.updateFields($('#Form_ItemEditForm_ImageType').val());
            },

            updateFields: function(selected) {
                // Hide all additional fields
                $('.field.additional').addClass('hidden');

                // Only show fields with the selected ImageType class (ie Basic, FullWidth etc).
                $('.field.' + selected).removeClass('hidden');
            }
        });

        /* Handle the Row block options */
        $('#Form_ItemEditForm_RowBlockColumnCount').entwine({
            onmatch: function () {
                this.updateFields($('#Form_ItemEditForm_RowBlockColumnCount').val());
            },

            onchange: function (e) {
                this.updateFields($('#Form_ItemEditForm_RowBlockColumnCount').val());
            },

            updateFields: function (selected) {
                if (selected === '2'){
                    //show ratio
                    $('#Form_ItemEditForm_RowBlockColumnRatio_Holder').removeClass('hidden');
                    return;
                }
                //hide ratio
                $('#Form_ItemEditForm_RowBlockColumnRatio_Holder').addClass('hidden');
            }
        });
    });
})(jQuery);
