# Education Blocks

A set of re-usable [Elemental](https://github.com/dnadesign/silverstripe-elemental)
blocks for Ministry of Education projects. This repository provides the model,
templates and basic styles but allows individual projects to stylize the blocks
as they need. 

## Table of Contents

1. [Examples](#example)
2. [Styling](#styling)
3. [Configuration](#configuration)

## Examples

### Banner

Full page width, image in the background and a H1 title, optional paragraph

### Hero 

Full page width title, colour and image.

![Hero](client/img/demos/hero.png)

### Tile

Designed to be embedded within an `ElementList` and links to another page.

![Tile](client/img/demos/tile.png)

## Mosaic Grid

Displays a grid of photos or links

![Mosaic](client/img/demos/mosaic.png)

## Chapter

A more structured content element which displays the content with a title and a
sidebar.

## Styling

To use the `scss` styles developed you can either include them one by one into
your file or include the all the styles by importing `contents.scss` 

```css
@import "../../vendor/education/blocks/client/scss/contents.scss";
```

## Configuration

### Disabling Blocks

If your education project does not require a particular block then disable it 
from the content editors.

```yml
Page:
  disallowed_elements:
    - Education\Blocks\Hero
```

## Copyright

Copyright 2018. All Rights Reserved Ministry of Education, NZ.
