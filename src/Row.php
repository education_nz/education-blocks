<?php

namespace Education\Blocks;

use DNADesign\ElementalList\Model\ElementList;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HeaderField;
use SilverStripe\Forms\LiteralField;

class Row extends ElementList
{
    private static $db = [
        'RowBlockColumnCount' => 'Enum("2, 3, 4, 5")',
        'RowBlockColumnRatio' => 'Enum("1-3, 1-2, 1-1, 2-1, 3-1")',
        'ContentAlignment' => 'Enum("top, center")',
    ];

    private static $defaults = [
        'RowBlockColumnCount' => '2',
        'RowBlockColumnRatio' => '1-1',
        'ContentAlignment' => 'top',
    ];

    private static $column_ratio_default = '1-1';

    private static $table_name = 'EducationBlock_Row';

    private static $singular_name = 'Row';

    private static $plural_name = 'Rows';

    private static $description = 'Configurable single row of elements';

    /**
     * Values here need to match the ContentAlignment Enum() defined in DB
     */
    private static $content_alignment = [
        'top' => 'Top',
        'center' => 'Center',
    ];

    public function getCMSFields()
    {
        $this->beforeUpdateCMSFields(function (FieldList $fields) {
            $fields->insertBefore('RowBlockColumnCount', HeaderField::create('SettingsTitle', 'Row settings'));
            $fields->dataFieldByName('RowBlockColumnCount')->setTitle('Column count');

            $fields->dataFieldByName('RowBlockColumnRatio')
                ->setTitle('Column ratio');

            $fields->addFieldsToTab('Root.Main', [
                DropdownField::create('ContentAlignment', 'Content alignment', self::$content_alignment),
                HeaderField::create('ElementsTitle', 'Row contents'),
                LiteralField::create('ElementsDetail', 'Only as many content items as specified in `Column count` will be used, starting from the top of the list')
            ]);
        });

        return parent::getCMSFields();
    }

    public function getHasRatio()
    {
        return (($this->RowBlockColumnCount == 2) && ($this->RowBlockColumnRatio !== self::$column_ratio_default));
    }

    public function getVisibleElements(){
        return $this->Elements()->Elements()->limit($this->RowBlockColumnCount);
    }

    public function getType()
    {
        return 'Row';
    }

    public function getColumnCount()
    {
        return $this->RowBlockColumnCount;
    }

    public function setColumnCount($columnCount)
    {
        $this->RowBlockColumnCount = $columnCount;
    }

    public function getColumnRatio()
    {
        return $this->RowBlockColumnRatio;
    }

    public function setColumnRatio($columnRatio)
    {
        $this->RowBlockColumnRatio = $columnRatio;
    }
}
