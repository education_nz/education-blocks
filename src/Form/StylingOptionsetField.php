<?php

namespace Education\Blocks\Form;

use SilverStripe\Forms\FormField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\Requirements;

class StylingOptionsetField extends OptionsetField
{
    protected $template = 'StylingOptionsetField';

    protected $fieldHolderTemplate = 'StylingOptionsetField_holder';

    public function Field($properties = [])
    {
        $options = [];
        $odd = false;

        foreach ($this->getSourceEmpty() as $value => $title) {
            $odd = !$odd;
            $options[] = $this->getFieldOption($value, $title, $odd);
        }

        $properties = array_merge($properties, [
            'Options' => new ArrayList($options),
        ]);

        return FormField::Field($properties);
    }
}
