<?php

namespace Education\Blocks;

use DNADesign\ElementalList\Model\ElementList;
use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\DropdownField;
use Heyday\ColorPalette\Fields\ColorPaletteField;
use SilverStripe\Dev\Debug;

class EnumerableList extends ElementList
{
    private static $table_name = 'EducationBlock_EnumerableList';

    private static $singular_name = 'Enumerable List';

    private static $plural_name = 'Enumerable Lists';

    private static $description = 'This block will hold a list of tiles or rows that will wrap onto multiple lines';

    private static $db = [
        'IncludeImages'     => 'Boolean',
        'ImageMode'         => 'Enum("cover, contain", "cover")',
        'ShowNumbers'       => 'Boolean',
        'HasBordersAndFill' => 'Boolean',
    ];

    private static $defaults = [
        'ImageMode' => 'cover',
    ];

    private static $allowed_elements = [
        'Education\Blocks\Tile',
        'Education\Blocks\Row',
    ];

    private static $image_modes = [
        'cover'   => 'Cover (good for pictures)',
        'contain' => 'Contain (good for icons and symbols)'
    ];

    public function getType()
    {
        return 'EnumerableList';
    }

    /**
     * Get the number of elements that the tile list
     * contains
     * 
     * @return Number
     */
    public function getLength()
    {
        return BaseElement::get()
            ->filter(['ParentID' => $this->Elements->ID])
            ->count();
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Main', [
            DropdownField::create('ImageMode', 'Image Mode', self::$image_modes),
        ]);

        return $fields;
    }
}
