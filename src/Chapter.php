<?php

namespace Education\Blocks;

use DNADesign\Elemental\Models\BaseElement;
use DNADesign\Elemental\Models\ElementalArea;
use DNADesign\Elemental\ElementalEditor;
use SilverStripe\Forms\LiteralField;
use Sheadawson\Linkable\Models\Link;
use Sheadawson\Linkable\Forms\LinkField;
use SilverStripe\Assets\Image;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Forms\FieldList;
use Heyday\ColorPalette\Fields\ColorPaletteField;

class Chapter extends BaseElement
{
    private static $db = [
        'Content' => 'HTMLText',
        'Subtitle' => 'Text'
    ];

    private static $has_one = [
        'Sidebar' => ElementalArea::class
    ];

    private static $defaults = [
        'ShowTitle' => 1,
    ];

    private static $owns = [
        'Sidebar'
    ];

    private static $cascade_duplicates = [
        'Sidebar'
    ];

    private static $allowed_sidebar_types = [

    ];

    private static $icon = 'font-icon-block-content';

    private static $description = 'Chapter Element';

    private static $table_name = 'EducationBlock_Chapter';

    private static $singular_name = 'Chapter';

    private static $plural_name = 'Chapter';

    public function getCMSFields()
    {
        $area = $this->Sidebar();
        $types = $this->config()->get('allowed_sidebar_types');
        $isInDb = $this->isInDB();
        $areaInDb = true;

        if (!$area->isInDb()) {
            $areaInDb = false;
            $area->write();

            $this->SidebarID = $area->ID;
            $this->write();
        }

        $this->beforeUpdateCMSFields(function (FieldList $fields) use ($area, $types, $isInDb, $areaInDb) {
            $editor = ElementalEditor::create('SidebarID', $area);

            if (!$isInDb || !$areaInDb) {
                $fields->removeByName('SidebarID');

                $fields->addFieldToTab('Root.Main', LiteralField::create('SidebarHelp',
                    '<p>You can create a sidebar once you save.</p>'
                ));
            }
            else if ($types) {
                $editor->setTypes($types);

                $fields->replaceField('SidebarID', $editor->getField());
            } else {
                $fields->removeByName('SidebarID');
            }
        });

        return parent::getCMSFields();
    }

    public function getSummary()
    {
        return DBField::create_field('HTMLText', $this->Content);
    }

    public function getType()
    {
        return 'Chapter';
    }
}
