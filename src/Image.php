<?php

namespace Education\Blocks;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Assets\Image as ImageFile;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextField;
use Sheadawson\Linkable\Models\Link;
use Sheadawson\Linkable\Forms\LinkField;
use SilverStripe\Forms\FieldList;
use Heyday\ColorPalette\Fields\ColorPaletteField;
use Education\Blocks\Helpers\BlockHelpers;

class Image extends BaseElement
{
    private static $db = [
        'ImageType' => "Enum('Basic, FullWidth, MosaicItem, CaptionOverlay', 'Basic')",
        'AltText' => "Varchar(255)",
        'TextColor' => "Enum('dark, light', 'dark')",
        'Caption' => "Varchar(255)",
        'ButtonText' => 'Varchar(255)',
        'AccentColour' => 'Varchar(255)',
    ];

    private static $has_one = [
        'Image' => ImageFile::class,
        'Link' => Link::class
    ];

    private static $owns = [
        'Image',
        'Link'
    ];

    private static $icon = 'font-icon-block-file';

    private static $singular_name = 'Image';

    private static $plural_name = 'Image';

    private static $table_name = 'EducationBlock_Image';

    /**
     * The $image_types array and the $image_default need to match the $db::'ImageType'
     * Enum if there's a better way to deal with this, I'm all ears.
     */
    private static $image_types = [
        'Basic' => 'Basic',
        'FullWidth' => 'Full Width Image',
        'MosaicItem' => 'Mosaic Image',
        'Basic' => 'Basic - Image actual size, no caption',
        'FullWidth' => 'Full width - Image fills container, caption underneath',
        'CaptionOverlay' => 'Caption overlay - Image fills container, caption overlays'
    ];

    /**
     * Values here need to match the TextColor Enum() defined in DB
     */
    private static $text_colors = [
        'dark' => 'Dark (Best for light images)',
        'light' => 'Light (Best for dark images)'
    ];

    private static $image_defaults = 'Basic';

    /**
     * We update the CMS form and add all the additional fields, we add all new fields as hidden by default.
     *
     * If a field is required for a particular ImageType, then add the ImageType Enum as a class.
     *
     * All fields that are NOT display by default on the Basic ImageType need to have the `additional` class applied.
     *
     * ie:
     *  class="field"                         -> will be displayed on all ImageTypes
     *  class="field additional FullWidth"    -> will be displayed when FullWidth ImageType is selected
     *  class="field additional"              -> will be a hidden field and not shown
     *
     * No additional JS etc should be required to manage the display of these fields if these rules are followed.
     *
     * @return \SilverStripe\Forms\FieldList
     */
    public function getCMSFields()
    {
        $colours = self::config()->get('colours');
        $backgrounds = self::config()->get('backgrounds');

        $this->beforeUpdateCMSFields(function (FieldList $fields) use ($colours, $backgrounds) {

            $fields->addFieldsToTab('Root.Main', [
                DropdownField::create('ImageType', 'Image display type', self::$image_types)
                    ->setDescription('Changing this option will alter the fields available and the display of
                    the image on the front end.'),

                TextField::create('Caption', 'Add a caption')
                    ->setDescription('Enter a caption for this image, if left blank no caption will be shown')
                    ->addExtraClass('additional FullWidth CaptionOverlay'),

                DropdownField::create('TextColor', 'Select the colour of the text', self::$text_colors)
                    ->setDescription('Specify the text color')
                    ->addExtraClass('additional CaptionOverlay'),

                TextField::create('ButtonText')
                    ->setDescription('Text for the call to action')
                    ->addExtraClass('additional MosaicItem'),

            ]);

            if ($colours) {
                $fields
                    ->replaceField('AccentColour', ColorPaletteField::create(
                        'AccentColour',
                        'Accent Colour',
                        $colours
                    )
                        ->setDescription('If there is an image then this will be the colour of the overlay, otherwise it will be the background colour of the block')
                        ->addExtraClass('additional MosaicItem'));
            } else {
                $fields->removeByName('AccentColour');
            }

            $fields
                ->replaceField('LinkID', LinkField::create('LinkID', 'Link')
                ->addExtraClass('additional MosaicItem'));

            BlockHelpers::altifyImageField($fields, 'Image', 'AltText');
        });

        return parent::getCMSFields();
    }

    public function Link($action = '')
    {
        return $this->getComponent('Link');
    }

    public function getType() {
        return 'Image';
    }

}
