<?php

namespace Education\Blocks\Interfaces;

/**
 * This interface is used in conjunction with the {@link PageTypeElement} block.
 *
 * A custom page type such as a search results page can provide the search
 * results as a Element to be positioned on the page. This interface allows any
 * page type to provide some rendered text.
 *
 * Example:
 *  SearchPage.php
 *      - BannerBlock
 *      - <PageTypeElement> <!-- dynamic block based on the page i.e results list
 *      - AccordionBlock
 *      - ...
 *
 * HOW TO USE:
 *  implement this interface on your model.
 *
 * <?php
 * use Education\Blocks\Interfaces\PageTypeElementProvider;
 *
 * class MySmartPage extends Page implements PageTypeElementProvider {
 *    public function getProvidedElements() {
 *      return ['Example Widget' => MySmartPageController::create($this)
 *          ->renderWith('Includes/ExampleWidget')];
 *    }
 *
 * }
 */
interface PageTypeElementProvider {

    /**
     * Returns an array of name for this element such as 'Search Results' and
     * the content for the elements.
     *
     * Note if you have methods you want to render from your controller then
     * make sure that your call creates and renders that controller rather than
     * the model class.
     *
     * @return string
     */
    public function getProvidedElements();
}
