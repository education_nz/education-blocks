<?php

namespace Education\Blocks\Extension;

use SilverStripe\Core\Extension;
use Education\Blocks\Interfaces\PageTypeElementProvider;
use Education\Blocks\PageTypeElement;

class ElementalEditorExtension extends Extension
{
    public function updateGetTypes($types)
    {
        if (isset($types[PageTypeElement::class])) {
            // if the page does not provide an element to render then hide the
            // option to create.
            if (!$this->owner->getArea()->Parent() instanceof PageTypeElementProvider) {
                unset($types[PageTypeElement::class]);
            } else {
                $types[PageTypeElement::class] = 'Page Type Element';
            }
        }
    }
}
