<?php

namespace Education\Blocks\Extension;

use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;

class BaseElementExtension extends DataExtension
{
    public function updateCMSFields(\SilverStripe\Forms\FieldList $fields)
    {
        $link = $this->owner->AbsoluteLink();

        $fields->addFieldsToTab(
            'Root.Main',
            TextField::create('Permalink', 'Permalink', $link)->setDisabled(true)
        );
    }
}
