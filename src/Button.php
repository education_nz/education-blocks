<?php

namespace Education\Blocks;

use DNADesign\Elemental\Models\BaseElement;
use Sheadawson\Linkable\Models\Link;
use Sheadawson\Linkable\Forms\LinkField;
use SilverStripe\Assets\Image;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Forms\FieldList;
use Heyday\ColorPalette\Fields\ColorPaletteField;

class Button extends BaseElement
{
    private static $has_one = [
        'Link' => Link::class
    ];

    private static $icon = 'font-icon-link';

    private static $description = 'Button';

    private static $table_name = 'EducationBlock_Button';

    private static $singular_name = 'Button';

    private static $plural_name = 'Buttons';

    public function getCMSFields()
    {
        $this->beforeUpdateCMSFields(function (FieldList $fields) {
            // better link function
            $fields->removeByName('Title');
            $fields->removeByName('TitleAndDisplayed');
            $fields->replaceField('LinkID', LinkField::create('LinkID', 'Link'));
        });

        return parent::getCMSFields();
    }

    public function getSummary()
    {
        return DBField::create_field('HTMLText', ($this->Link()) ? $this->Link()->getLinkURL() : '');
    }

    public function Link($action = '')
    {
        return $this->getComponent('Link');
    }

    public function getTitle()
    {
        return ($this->Link()) ? $this->Link()->Title : 'Button';
    }

    public function getType()
    {
        return 'Button';
    }
}
