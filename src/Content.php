<?php

namespace Education\Blocks;

use DNADesign\Elemental\Models\ElementContent;

/**
 * Class Content
 * A wrapper around ElementContent so that we can override the template in the same folder structure
 *
 * @package Education\Blocks
 */
class Content extends ElementContent
{
    private static $table_name = 'EducationBlock_Content';

    private static $singular_name = 'Content block';

    private static $plural_name = 'Content blocks';

    private static $description = 'HTML content block';
}
