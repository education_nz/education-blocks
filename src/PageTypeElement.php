<?php

namespace Education\Blocks;

use DNADesign\Elemental\Models\BaseElement;
use Education\Blocks\Interfaces\PageTypeElementProvider;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\DropdownField;
/**
 * This is used in conjunction with the {@link PageTypeElementProvider}
 * interface.
 *
 * Take for instance a custom page type which provides a piece of content such
 * as a filterable paginated list this element allows
 */
class PageTypeElement extends BaseElement
{
    private static $db = [
        'ElementKey' => 'Varchar'
    ];

    private static $icon = 'font-icon-block-layout';

    private static $description = 'Page Type Element';

    private static $table_name = 'EducationBlock_PageTypeElement';

    private static $singular_name = 'Page Type Element';

    private static $plural_name = 'Page Type Elements';

    public function getCMSFields()
    {
        $page = $this->getPage();

        if ($page && $page instanceof PageTypeElementProvider) {
            $elements = array_keys($page->getProvidedElements());

            $this->beforeUpdateCMSFields(function (FieldList $fields) use ($elements) {
                $fields->addFieldToTab('Root.Main', DropdownField::create(
                    'ElementKey',
                    'Render Element',
                    array_combine($elements, $elements)
                ));
            });
        } else {
            $this->beforeUpdateCMSFields(function (FieldList $fields) {
                $fields->makeFieldReadonly('ElementKey');
                $fields->addFieldToTab('Root.Main', LiteralField::create('PageError', '<p>Will not display, current page does not implement any valid types.</p>'));
            });
        }

        return parent::getCMSFields();
    }

    public function getSummary()
    {
        return '';
    }

    public function getType()
    {
        return 'Page Type Element';
    }

    public function getRenderPageElement()
    {
        $page = $this->getPage();

        if (!$page) {
            return;
        }

        $elements = $page->getProvidedElements();

        if ($this->ElementKey && isset($elements[$this->ElementKey])) {
            return $elements[$this->ElementKey];
        } else if (count($elements) === 1) {
            return array_shift($elements);
        }

        return '';

    }
}
