<?php

namespace Education\Blocks;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxSetField;
use CWP\CWP\PageTypes\DatedUpdateHolder;
use Sheadawson\Linkable\Models\Link;
use Sheadawson\Linkable\Forms\LinkField;

class News extends BaseElement
{
    private static $icon = 'font-icon-block-content';

    private static $singular_name = 'News';

    private static $plural_name = 'News';

    private static $table_name = 'EducationBlock_News';

    private static $db = [
        'DisplayNumber' => 'Int',
    ];

    private static $has_one = [
        'PullNewsFrom' => DatedUpdateHolder::class,
        'MoreNews' => Link::class
    ];

    private static $defaults = [
        'DisplayNumber' => 4
    ];

    public function getCMSFields()
    {
        $this->beforeUpdateCMSFields(function (FieldList $fields) {
            $fields->replaceField('MoreNewsID', LinkField::create('MoreNewsID', 'More News Link'));
        });

        $fields = parent::getCMSFields();

        return $fields;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'News Feed';
    }

    /**
     *
     * @return DataList
     */
    public function getNewsArticles()
    {
        return $this->PullNewsFrom()->Updates()->limit($this->DisplayNumber);
    }
}
