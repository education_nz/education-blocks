<?php

namespace Education\Blocks;

use DNADesign\Elemental\Models\BaseElement;
use Sheadawson\Linkable\Models\Link;
use Sheadawson\Linkable\Forms\LinkField;
use SilverStripe\Assets\Image;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Forms\FieldList;
use Heyday\ColorPalette\Fields\ColorPaletteField;
use Education\Blocks\Helpers\BlockHelpers;

class Tile extends BaseElement
{
    private static $db = [
        'Content' => 'HTMLText',
        'AscentColour' => 'Varchar(255)',
        'ImageAlt' => 'Varchar(255)'
    ];

    private static $has_one = [
        'Image' => Image::class,
        'Link' => Link::class
    ];

    private static $owns = [
        'Image'
    ];

    private static $defaults = [
        'ShowTitle' => true
    ];

    private static $icon = 'font-icon-block-content';

    private static $description = 'Tile Element Image and Text';

    private static $table_name = 'EducationBlock_Tile';

    private static $singular_name = 'Tile';

    private static $plural_name = 'Tiles';

    public function getCMSFields()
    {
        $backgrounds = self::config()->get('colours');

        $this->beforeUpdateCMSFields(function (FieldList $fields) use ($backgrounds) {
            BlockHelpers::altifyImageField($fields, 'Image', 'ImageAlt');

            // better link function
            $fields->replaceField('LinkID', LinkField::create('LinkID', 'Link'));

            // better colour picker
            if ($backgrounds) {
                $fields->replaceField('AscentColour', ColorPaletteField::create(
                    'AscentColour',
                    'Accent Colour',
                    $backgrounds
                ));
            } else {
                $fields->removeByName('AscentColour');
            }
        });

        return parent::getCMSFields();
    }

    public function getSummary()
    {
        return DBField::create_field('HTMLText', $this->Content);
    }

    public function getType()
    {
        return 'Tile';
    }

    public function Link($action = NULL)
    {
        if ($obj = $this->getComponent('Link')) {
            return $obj->getLinkURL();
        }

        return parent::Link($action);
    }

    public function LinkTargetAttr()
    {
        return $this->getComponent('Link')->getTargetAttr();
    }

    public function LinkTitle()
    {
        return $this->getComponent('Link')->Title;
    }

    /**
     * Overriding can view to hack a bug where
     * these tiles don't show when not logged in.
     * Would be great if someone one day can figure out the cause of this issue...
     * and fix it in a less gross way.
     */
    public function canView($member = null)
    {
        return true;
    }
}
