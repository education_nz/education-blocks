<?php

namespace Education\Blocks;

use DNADesign\ElementalList\Model\ElementList;

class Mosaic extends ElementList
{
    private static $db = [
        'ItemsPerRow' => "Enum('2, 3, 4, 5')"
    ];

    private static $table_name = 'EducationBlock_Mosaic';

    private static $singular_name = 'Mosaic';

    private static $plural_name = 'Mosaics';

    private static $allowed_elements = [
        'Education\Blocks\Image'
    ];

    public function getType()
    {
        return 'Mosaic';
    }
}
