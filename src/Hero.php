<?php

namespace Education\Blocks;

use DNADesign\Elemental\Models\BaseElement;
use Sheadawson\Linkable\Models\Link;
use Sheadawson\Linkable\Forms\LinkField;
use SilverStripe\Assets\Image;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use Heyday\ColorPalette\Fields\ColorPaletteField;
use Education\Blocks\Helpers\BlockHelpers;

class Hero extends BaseElement
{
    private static $db = [
        'Subtitle' => 'Varchar(255)',
        'Caption' => 'Text',
        'Content' => 'HTMLText',
        'HeadingLevel' => 'Int',
        'BackgroundColour' => 'Varchar(255)',
        'ImageAlt' => 'Varchar(255)',
        'ImageAlign' => 'Enum("floating, horizon, sink", "floating")'
    ];

    private static $has_one = [
        'Image' => Image::class,
        'Link' => Link::class
    ];

    private static $owns = [
        'Image'
    ];

    private static $defaults = [
        'ShowTitle' => 1,
        'HeadingLevel' => 2
    ];

    private static $icon = 'font-icon-block-banner';

    private static $description = 'Hero Header with Image and Text';

    private static $table_name = 'EducationBlock_Hero';

    private static $singular_name = 'Hero';

    private static $plural_name = 'Heros';

    public function getCMSFields()
    {
        $backgrounds = self::config()->get('backgrounds');

        $this->beforeUpdateCMSFields(function (FieldList $fields) use ($backgrounds) {
            // better link function
            $fields->replaceField('LinkID', LinkField::create('LinkID', 'Link'));

            // Dropdown for heading level, if this is the first block we should use h1 for semantics.
            $fields->replaceField('HeadingLevel', DropdownField::create('HeadingLevel', 'Heading Level', [
                1 => '1',
                2 => '2',
                3 => '3',
                4 => '4',
                5 => '5',
                6 => '6'
            ], 2));

            // put caption after image
            $caption = $fields->dataFieldByName('Caption');

            $fields->removeByName('Caption');
            $fields->insertAfter('Image', $caption);

            BlockHelpers::altifyImageField($fields, 'Image', 'ImageAlt');

            // better colour picker
            if ($backgrounds) {
                $fields->replaceField('BackgroundColour', ColorPaletteField::create(
                    'BackgroundColour',
                    'Background Colour',
                    $backgrounds
                ));
            } else {
                $fields->removeByName('BackgroundColour');
            }

            $imageAlign = $fields->dataFieldByName('ImageAlign');

            if ($imageAlign) {
                $imageAlign->setDescription(
                    'Choose how you want the image to sit within the banner, floating (not aligned top or bottom), horizon (aligned strictly to the bottom) or sink (-40px sunk off the bottom)'
                );
            }
        });

        return parent::getCMSFields();
    }

    public function getSummary()
    {
        return DBField::create_field('HTMLText', $this->Subtitle);
    }

    public function getType()
    {
        return 'Hero';
    }

    public function Link($action = '')
    {
        return $this->getComponent('Link');
    }
}
