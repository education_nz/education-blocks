<?php

namespace Education\Blocks;

use DNADesign\Elemental\Models\BaseElement;
use Sheadawson\Linkable\Models\Link;
use Sheadawson\Linkable\Forms\LinkField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Forms\FieldList;
use Heyday\ColorPalette\Fields\ColorPaletteField;

class Heading extends BaseElement
{
    private static $defaults = [
        'ShowTitle' => 1
    ];

    private static $icon = 'font-icon-block-banner';

    private static $description = 'Page section heading';

    private static $table_name = 'EducationBlock_Heading';

    private static $singular_name = 'Heading';

    private static $plural_name = 'Headings';

    public function getCMSFields()
    {
        $this->beforeUpdateCMSFields(function (FieldList $fields) {
            $fields->removeByName('Title');
            $fields->removeByName('TitleAndDisplayed');
            $fields->addFieldToTab('Root.Main', TextField::create('Title', 'Heading Text'));
        });

        return parent::getCMSFields();
    }

    public function getSummary()
    {
        return DBField::create_field('HTMLText', $this->Title);
    }

    public function getType()
    {
        return 'Heading';
    }
}
