<?php

namespace Education\Blocks\Helpers;

/**
 * Collection of helper methods for dealing with blocks.
 */
class BlockHelpers
{
    /**
     * @param SilverStripe\Forms\FieldList $fields
     * @param string $imageFieldName
     * @param string $altFieldName
     */
    public static function altifyImageField($fields, $imageFieldName, $altFieldName)
    {
        $alt = $fields->dataFieldByName($altFieldName);

        $fields->removeByName($altFieldName);

        if ($alt) {
            $alt->setTitle('');
            $alt->setAttribute('placeholder', 'Alt Text');
            $alt->setDescription('An alt tag is an HTML attribute applied to image tags to provide a text alternative for search engines. Applying images can positively impact search engine rankings.');

            $fields->insertAfter($imageFieldName, $alt);
        }
    }
}
