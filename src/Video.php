<?php

namespace Education\Blocks;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxSetField;
use SilverStripe\ORM\FieldType\DBField;
use CWP\CWP\PageTypes\DatedUpdateHolder;
use Embed\Embed;
use Exception;
use Psr\Log\LoggerInterface;
use SilverStripe\Core\Injector\Injector;

class Video extends BaseElement
{
    private static $icon = 'font-icon-block-media';

    private static $singular_name = 'Video';

    private static $plural_name = 'Video';

    private static $table_name = 'EducationBlock_Video';

    private static $db = [
        'EmbedURL' => 'Varchar(255)',
        'CaptionText' => 'HTMLText',
        'VideoPosition' => 'Enum("Left, Right, Center, Center-Wide")',
        'Transcript' => 'HTMLText',
    ];

    private static $casting = [
        'getVideoPositionClass' => 'Varchar'
    ];

    public function getCMSFields()
    {
        $this->beforeUpdateCMSFields(function (FieldList $fields) {

        });

        $fields = parent::getCMSFields();

        return $fields;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'Video';
    }

    /**
     * @return string
     */
    public function getVideoPositionClass()
    {
        return strtolower($this->dbObject('VideoPosition')->getValue());
    }

    /**
     * @return HTMLText
     */
    public function getEmbed()
    {
        if (!$this->EmbedURL) {
            return null;
        }

        try {
            $info = Embed::create($this->EmbedURL, [
                'oembed' => [
                    'parameters' => [
                        'width' => false,
                        'height' => false,
                        'title' => 0,
                        'byline' => 0,
                        'portrait' => 0,
                        'badge' => 0
                    ],
                ]
            ]);
        } catch (Exception $e) {
            Injector::inst()->get(LoggerInterface::class)
                ->notice($e->getMessage(), ['exception' => $e]);

            return null;
        }

        return DBField::create_field('HTMLText', $info->code);
    }
}
