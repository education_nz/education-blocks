<div class="block-news">
    <% if ShowTitle %>
        <div class="wrapper">
            <hr />
            <h2>$Title</h2>
        </div>
    <% end_if %>

    <div class="wrapper">
        <div class="block-news__list">
            <% loop NewsArticles %>
                <a href="$Link" class="block-news__article <% if Imported %>block-news__article--imported<% end_if %>" <% if Imported %>target="_blank"<% end_if %>>
                    <p class="block-news__date">$Date.Format(d MMMM YYYY)</p>
                    <h3>$Title</h3>

                    <p class="block-news__abstract">$Abstract.LimitCharacters(100)</p>
                </a>
            <% end_loop %>
        </div>
    </div>

    <div class="wrapper block-news__more">
        <a href="$MoreNews.LinkURL" class="btn">$MoreNews.Title</a>
    </div>
</div>
