<div class="block-row">
    <% if ShowTitle %><div class="block-row__title">$Title</div><% end_if %>
    <div class="block-row__items <% if ContentAlignment == 'center' %>block-row__items--centered <% end_if %><% if HasRatio %>block-row__items--$RowBlockColumnRatio<% end_if %>">
        <% if $VisibleElements %>
            <% loop $VisibleElements %>
                $Me
            <% end_loop %>
        <% end_if %>
    </div>
</div>
